﻿namespace RowanWillis.Cqrs;

internal static class TypeExtensions
{
	internal static string GenericName(this Type type)
	{
		if (type is null)
			throw new ArgumentNullException(nameof(type));

		var name = type.Name;

		if (!type.IsGenericType)
			return name;
		
		name = type.Name[..type.Name.IndexOf('`')];
		var genericTypes = type.GenericTypeArguments;
		name = $"{name}<{string.Join(",", genericTypes.Select(GenericName))}>";

		return name;
	}
}