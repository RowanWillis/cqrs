﻿namespace RowanWillis.Cqrs;

public class CommandHandlerNotImplementedException : Exception
{
    public Type CommandType { get; }

    public CommandHandlerNotImplementedException(Type commandType)
        : base(message: $"No handler found for command of type {commandType.GenericName()}.")
    {
        CommandType = commandType;
    }
}
