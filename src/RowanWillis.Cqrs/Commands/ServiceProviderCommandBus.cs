﻿using Microsoft.Extensions.DependencyInjection;

namespace RowanWillis.Cqrs;

public class ServiceProviderCommandBus : ICommandBus
{
    private readonly IServiceProvider Services;

    public ServiceProviderCommandBus(IServiceProvider services)
    {
        Services = services;
    }

    public async Task<TResult> Dispatch<TCommand, TResult>(
        TCommand command) where TCommand : ICommand<TResult>
    {
        var handler = Services.GetService<ICommandHandler<TCommand, TResult>>();

        if (handler is null)
            throw new CommandHandlerNotImplementedException(typeof(TCommand));

        return await handler.Handle(command);
    }
}
