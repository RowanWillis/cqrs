﻿namespace RowanWillis.Cqrs;

public interface ICommandBus
{
	Task<TResult> Dispatch<TCommand, TResult>(TCommand command)
		where TCommand : ICommand<TResult>;
}
