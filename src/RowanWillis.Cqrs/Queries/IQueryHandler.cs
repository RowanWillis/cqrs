﻿namespace RowanWillis.Cqrs;

public interface IQueryHandler<in TQuery, TResult>
	where TQuery : IQuery<TResult>
{
	Task<TResult> Handle(TQuery query);
}