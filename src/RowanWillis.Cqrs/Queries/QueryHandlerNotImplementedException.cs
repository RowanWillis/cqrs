﻿namespace RowanWillis.Cqrs;

public class QueryHandlerNotImplementedException : Exception
{
    public Type QueryType { get; }

    public QueryHandlerNotImplementedException(Type queryType)
        : base(message: $"No handler found for query of type {queryType.GenericName()}.")
    {
        QueryType = queryType;
    }
}