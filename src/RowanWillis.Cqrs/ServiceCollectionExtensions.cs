﻿using Microsoft.Extensions.DependencyInjection;

namespace RowanWillis.Cqrs;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCqrs(this IServiceCollection services)
       => services
           .AddScoped<IQueryBus, ServiceProviderQueryBus>()
           .AddScoped<ICommandBus, ServiceProviderCommandBus>();

    public static IServiceCollection AddCommandHandler<TCommand, TResult, THandler>(this IServiceCollection services)
        where TCommand : ICommand<TResult>
        where THandler : class, ICommandHandler<TCommand, TResult>
        => services.AddScoped<ICommandHandler<TCommand, TResult>, THandler>();

    public static IServiceCollection AddQueryHandler<TQuery, TResult, THandler>(this IServiceCollection services)
        where TQuery : IQuery<TResult>
        where THandler : class, IQueryHandler<TQuery, TResult>
        => services.AddScoped<IQueryHandler<TQuery, TResult>, THandler>();
}