﻿using RowanWillis.Cqrs;
using Xunit;

namespace RowanWillis.Cqrs.UnitTests;

public class TypeExtensionsTests
{
	[Theory]
	[InlineData(typeof(int))]
	[InlineData(typeof(string))]
	[InlineData(typeof(object))]
	[InlineData(typeof(object[]))]
	[InlineData(typeof(IDisposable))]
	public void GivenANonGenericType_GenericName_ReturnsTheSimpleTypeName(Type type)
	{
		var genericTypeName = type.GenericName();
		Assert.Equal(type.Name, genericTypeName);
	}

	[Theory]
	[InlineData(typeof(List<string>), "List<String>")]
	[InlineData(typeof(IDictionary<int, object>), "IDictionary<Int32,Object>")]
	[InlineData(typeof(Func<IComparable, bool>), "Func<IComparable,Boolean>")]
	public void GivenAGenericType_GenericName_DescribesTheTypeWithAllParameters(Type type, string expectedValue)
	{
		var genericTypeName = type.GenericName();
		Assert.Equal(expectedValue, genericTypeName);
	}

	[Theory]
	[InlineData(typeof(List<List<object>>), "List<List<Object>>")]
	[InlineData(typeof(IDictionary<List<List<string>>, HashSet<object>>), "IDictionary<List<List<String>>,HashSet<Object>>")]
	[InlineData(typeof(Func<Dictionary<string, List<string>>, long>), "Func<Dictionary<String,List<String>>,Int64>")]
	public void GivenANestedGenericType_GenericName_DescribesTheTypeWithAllNestedParameters(Type type, string expectedValue)
	{
		var genericTypeName = type.GenericName();
		Assert.Equal(expectedValue, genericTypeName);
	}

	[Fact]
	public void GivenNull_GenericName_ThrowsArgumentNullException()
	{
	#nullable disable
		Type type = null;
		_ = Assert.Throws<ArgumentNullException>(() => type.GenericName());
	#nullable enable
	}
}