﻿using Moq;
using Xunit;

namespace RowanWillis.Cqrs.UnitTests;

public class ServiceProviderQueryBusTests
{
    // ReSharper disable once MemberCanBePrivate.Global
    public record TestQuery : IQuery<TestQueryResult>;

    // ReSharper disable once MemberCanBePrivate.Global
    public record TestQueryResult;

    [Fact]
    public async Task GivenAHandlerIsImplemented_WhenAQueryIsDispatched_ThenTheHandlerResultIsReturned()
    {
        var expectedResult = new TestQueryResult();
        var query = new TestQuery();

        var handler = new Mock<IQueryHandler<TestQuery, TestQueryResult>>();
        _ = handler.Setup(_ => _.Handle(query)).ReturnsAsync(expectedResult);

        var serviceProvider = new Mock<IServiceProvider>();
        _ = serviceProvider
            .Setup(_ => _.GetService(typeof(IQueryHandler<TestQuery, TestQueryResult>)))
            .Returns(handler.Object);

        var queryBus = new ServiceProviderQueryBus(serviceProvider.Object);

        var result = await queryBus.Dispatch<TestQuery, TestQueryResult>(query);

        Assert.Equal(expectedResult, result);
    }

    [Fact]
    public async Task GivenNoHandlerIsImplemented_WhenAQueryIsDispatched_ThenAnExceptionIsThrown_WhichIndicatesTheQueryType()
    {
        var query = new TestQuery();

        var serviceProvider = new Mock<IServiceProvider>();
        _ = serviceProvider
            .Setup(_ => _.GetService(typeof(IQueryHandler<TestQuery, TestQueryResult>)))
            .Returns(null);

        var queryBus = new ServiceProviderQueryBus(serviceProvider.Object);

        var exception = await Assert.ThrowsAsync<QueryHandlerNotImplementedException>(
            () => queryBus.Dispatch<TestQuery, TestQueryResult>(query));

        Assert.Equal(typeof(TestQuery), exception.QueryType);
        Assert.Contains(nameof(TestQuery), exception.Message);
    }
}