﻿using Moq;
using Xunit;

namespace RowanWillis.Cqrs.UnitTests;

public class ServiceProviderCommandBusTests
{

    // ReSharper disable once MemberCanBePrivate.Global
    public record TestCommand : ICommand<TestCommandResult>;

    // ReSharper disable once MemberCanBePrivate.Global
    public record TestCommandResult;

    [Fact]
    public async Task GivenAnImplementedCommand_WhenDispatched_ThenTheResultIsReturned()
    {
        var expectedResult = new TestCommandResult();
        var command = new TestCommand();

        var handler = new Mock<ICommandHandler<TestCommand, TestCommandResult>>();
        _ = handler.Setup(_ => _.Handle(command)).ReturnsAsync(expectedResult);

        var serviceProvider = new Mock<IServiceProvider>();
        _ = serviceProvider
            .Setup(_ => _.GetService(typeof(ICommandHandler<TestCommand, TestCommandResult>)))
            .Returns(handler.Object);

        var commandBus = new ServiceProviderCommandBus(serviceProvider.Object);

        var result = await commandBus.Dispatch<TestCommand, TestCommandResult>(command);

        Assert.Equal(expectedResult, result);
    }

    [Fact]
    public async Task GivenANonImplementedCommand_WhenDispatched_ThenACommandHandlerNotImplementedExceptionIsThrown()
    {
        var query = new TestCommand();

        var serviceProvider = new Mock<IServiceProvider>();
        _ = serviceProvider
            .Setup(_ => _.GetService(typeof(ICommandHandler<TestCommand, TestCommandResult>)))
            .Returns(null);

        var commandBus = new ServiceProviderCommandBus(serviceProvider.Object);

        var exception = await Assert.ThrowsAsync<CommandHandlerNotImplementedException>(
            () => commandBus.Dispatch<TestCommand, TestCommandResult>(query));

        Assert.Equal(typeof(TestCommand), exception.CommandType);
        Assert.Contains(nameof(TestCommand), exception.Message);
    }
}